# Integration Test for Project Service

## Goal

This repo is used for the integration test between project service and mysql database.
More specifically, we want to test binus-student-project-showcase/svc-project and a mysql service where the db is persisted in the senyum-id/vol-svc-project.

## How-to

1. Clone binus-student-project-showcase/svc-project

    ```
    git clone git@gitlab.com:binus-student-project-showcase/svc-project.git
    ```

2. Clone binus-student-project-showcase/vol-svc-project

    ```
    git clone git@gitlab.com:binus-student-project-showcase/vol-svc-project.git
    ```

3. Clone binus-student-project-showcase/svc-auth

    ```
    git clone git@gitlab.com:binus-student-project-showcase/svc-auth.git
    ```

4. Make sure that your dir structure looks like the following

    ```
    .
    ├── docker-compose.yml
    ├── README.md
    ├── svc-auth
    ├── svc-project
    └── vol-svc-project
    ```

4. Find the string '{SERVERNAME}' in the docker-compose.yaml file, and replace it accordingly

5. To run

    ```
    sudo docker-compose up --build -d
    ```

6. To check

    ```
    sudo docker-compose ps
    ```

7. To stop

    ```
    sudo docker-compose stop
    ```

## Test

1. Visit http://localhost:8080/ and try the swagger to test it